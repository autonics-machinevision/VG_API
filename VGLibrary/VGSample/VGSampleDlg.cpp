﻿
// VGSampleDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "VGSample.h"
#include "VGSampleDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVGSampleDlg 대화 상자

VGTotalResult _result;
CVGSampleDlg* m_pMain;

CVGSampleDlg::CVGSampleDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_VGSAMPLE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pBitmapInfo = NULL;
	m_pBuffer = NULL;
	m_nSizeX = 752;
	m_nSizeY = 480;
	m_pMain = this;
}

void CVGSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE, m_cPictureBox);
}

BEGIN_MESSAGE_MAP(CVGSampleDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CVGSampleDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CVGSampleDlg::OnBnClickedCancel)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDSTOP, &CVGSampleDlg::OnBnClickedStop)
	ON_BN_CLICKED(IDSNAP, &CVGSampleDlg::OnBnClickedSnap)
	ON_BN_CLICKED(IDLIVE, &CVGSampleDlg::OnBnClickedLive)
END_MESSAGE_MAP()


// CVGSampleDlg 메시지 처리기

void CVGSampleDlg::Connect()
{
	_handle = vgOpenByIP("169.254.63.3");

	VGInfo* info = (VGInfo*)_handle;
	if (_handle
		!= NULL)
	{
		printf("connect success\n");

		CreateBmpInfo(m_nSizeX, m_nSizeY, 24);
		m_pBuffer = new BYTE[m_nSizeX * m_nSizeY * 3];
	}
	else
	{
		printf("connect fail\n");
	}
}

void CVGSampleDlg::DisConnect()
{
	vgClose(_handle);
}

void OnReceiveInspectionResult(VGHANDLE handle, VGTotalResult result)
{
	_result = result;
	memcpy(m_pMain->m_pBuffer, _result.VGImage.PixelData, _result.VGImage.Width * _result.VGImage.Height * 3);
	m_pMain->Invalidate();	
}

BOOL CVGSampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	

	GetDlgItem(IDC_IMAGE)->GetClientRect(&m_rectDisplay);

	Connect();

	vgSetReceiveCallback(OnReceiveInspectionResult, _handle);	

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVGSampleDlg::OnPaint()
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();

		CClientDC cdc(GetDlgItem(IDC_IMAGE));



		//memset(m_pBuffer, 0, 752 * 480 * 3);

		SetStretchBltMode(cdc.GetSafeHdc(), HALFTONE);
		StretchDIBits(cdc.GetSafeHdc(), 0, 0, m_rectDisplay.Width(), m_rectDisplay.Height(), 0, 0, _result.VGImage.Width, _result.VGImage.Height, m_pBuffer, m_pBitmapInfo, DIB_RGB_COLORS, SRCCOPY);

		double xratio = (double)m_rectDisplay.Width() / 752.0;
		double yratio = (double)m_rectDisplay.Height() / 480.0;

		int x = (double)_result.MatchPosX * xratio;
		int y = (double)_result.MatchPosY * yratio;		

		CPen pen;
		pen.CreatePen(PS_SOLID, 3, RGB(255, 0, 0));
		cdc.SelectObject(&pen);

		if (x > 0 || y > 0)
		{
			cdc.MoveTo(x - 20, y);
			cdc.LineTo(x + 20, y);
			cdc.MoveTo(x, y - 20);
			cdc.LineTo(x, y + 20);
		}


	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVGSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CVGSampleDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (_handle == NULL)
	{
		printf("handle is null");
		return;
	}

	vgSetOperationMode(_handle, BUSY);	
}

void CVGSampleDlg::OnBnClickedStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	vgSetOperationMode(_handle, IDLE);
}

void CVGSampleDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_pBitmapInfo)
		delete[] m_pBitmapInfo;
	m_pBitmapInfo = NULL;

	if (m_pBuffer)
		delete m_pBuffer;
	m_pBuffer = NULL;



	DisConnect();
	CDialog::OnCancel();
}


void CVGSampleDlg::CreateBmpInfo(int nWidth, int nHeight, int nBpp)
{
	if (m_pBitmapInfo != NULL)
		delete[] m_pBitmapInfo;
	m_pBitmapInfo = NULL;

	if (nBpp == 8)
		m_pBitmapInfo = (BITMAPINFO *) new BYTE[sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD)];
	else if (nBpp == 24)
		m_pBitmapInfo = (BITMAPINFO *) new BYTE[sizeof(BITMAPINFO)];

	m_pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_pBitmapInfo->bmiHeader.biPlanes = 1;
	m_pBitmapInfo->bmiHeader.biBitCount = nBpp;
	m_pBitmapInfo->bmiHeader.biCompression = BI_RGB;

	if (nBpp == 8)
		m_pBitmapInfo->bmiHeader.biSizeImage = 0;
	else if (nBpp == 24)
		m_pBitmapInfo->bmiHeader.biSizeImage = (((nWidth * 24 + 31) & ~31) >> 3) * nHeight;

	m_pBitmapInfo->bmiHeader.biXPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biClrUsed = 0;
	m_pBitmapInfo->bmiHeader.biClrImportant = 0;

	if (nBpp == 8)
	{
		for (int i = 0; i < 256; i++)
		{
			m_pBitmapInfo->bmiColors[i].rgbBlue = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbGreen = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbRed = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbReserved = 0;
		}
	}

	m_pBitmapInfo->bmiHeader.biWidth = nWidth;
	m_pBitmapInfo->bmiHeader.biHeight = -nHeight;
}





void CVGSampleDlg::OnBnClickedSnap()
{
	vgCapture(_handle);
}


void CVGSampleDlg::OnBnClickedLive()
{
	vgCaptureLive(_handle, true);
}

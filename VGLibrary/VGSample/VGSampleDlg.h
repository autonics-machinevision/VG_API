﻿
// VGSampleDlg.h: 헤더 파일
//

#pragma once

// CVGSampleDlg 대화 상자
class CVGSampleDlg : public CDialog
{
// 생성입니다.
public:
	CVGSampleDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VGSAMPLE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
	BITMAPINFO*	m_pBitmapInfo;
	CRect m_rectDisplay;
	int m_nSizeX, m_nSizeY;
	BYTE*		m_pBuffer;

private:
	VGHANDLE _handle;
	
	void Connect();
	void DisConnect();
	void CreateBmpInfo(int nWidth, int nHeight, int nBpp);

	// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	//void OnReceiveInspectionResult(VGHANDLE handle, VGTotalResult result);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CStatic m_cPictureBox;
	afx_msg void OnBnClickedStop();
	afx_msg void OnBnClickedSnap();
	afx_msg void OnBnClickedLive();
};
extern CVGSampleDlg* m_pMain;


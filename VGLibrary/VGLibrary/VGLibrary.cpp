﻿#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <thread>
#include <math.h>
#include <vector>
#include "VGLibrary.h"
#include <Windows.h>

using namespace std;

void ErrorHandling(const char* message);

char ipAddress[] = "169.254.63.3";

#define PORT 8000
#define MAX_TRANS_BUFFER_SIZE  1024 * 1024
#define HEADER_SIZE 8 + 4 + 4 + 4 + 8
#define DATvgSIZE 4
#define PACKET_SIZE HEADER_SIZE + DATvgSIZE
#define CS_HAND_SHAKE 100
#define CS_WORK_GROUP_READ 402
#define IMAGE_WIDTH 752
#define IMAGE_HEIGHT 480




struct PacketHeader
{
	BYTE StartCode[8];
	int PacketSize;
	int Message;
	int DataSize;	
};

struct PacketBody
{
	int Data;
};

struct PacketTail
{
	BYTE EndCode[8];
};

struct PacketBodyResultStart
{
	int saveLocation;
	int ImageWidth;
	int ImageHeight;
	BYTE PixelData[752 * 480];
	int TriggerPassCount;
	int TriggerFailCount;
	int WorkCount;	
};

struct PacketBodyResultMiddle
{
	WORK_TYPE WorkType;	// work data start
	VGInspectionResult result;	
};
struct PacketBodyResultEnd
{
	int Output[4];
	int FirstRho;
	int FirstVotes;
	int FirstTheta;
	int SecondRho;
	int SecondVotes;
	int SecondTheta;
	int TotalProcessTime;
	int TotalPassCount;
	int TotalFailCount;
};

struct PacketGeneral
{
	PacketHeader header;
	PacketBody body;
	PacketTail tail;
};

struct PacketResult
{
	PacketBodyResultStart bodyStart;
	PacketBodyResultMiddle bodyMiddle;
	PacketBodyResultEnd bodyEnd;
};

class MemoryReadWriter
{
public:
	MemoryReadWriter(void* stream, int size)
	{
		SetStream(stream, size);
	}

	MemoryReadWriter(int allocSize)
	{
		allocate(allocSize);
	}

	~MemoryReadWriter()
	{
		release();
	}

public:
	void SetStream(void* stream, int size)
	{
		release();

		streamPtr = (BYTE*)stream;
		streamSize = size;
		position = 0;
		isAllocated = false;
	}

	void allocate(int allocSize)
	{
		release();

		streamPtr = new BYTE[allocSize];
		streamSize = allocSize;
		position = 0;
		isAllocated = true;

	}

	void release()
	{
		if (isAllocated)
		{
			delete[] streamPtr;
		}

		streamPtr = nullptr;
		streamSize = 0;
		position = -1;
		isAllocated = false;
	}

	const BYTE* getPtr()
	{
		return streamPtr;
	}

	int getSize()
	{
		return position;
	}

	void resetPosition()
	{
		position = 0;
	}

public:
	int WriteByte(BYTE value)
	{
		if (position > streamSize) return -1;

		streamPtr[position] = value;

		position++;

		return position;
	}

	int WriteBytes(BYTE* valuePtr, int count)
	{
		if (position > streamSize || count > streamSize - position) return -1;
		
		memcpy(&streamPtr[position], valuePtr, count);

		position += count;

		return position;
	}

	int WriteInt32(int value)
	{
		if (position > streamSize) return -1;

		memcpy(&streamPtr[position], &value, sizeof(value));

		position += sizeof(value);

		return position;
	}

public:
	BYTE ReadByte()
	{
		BYTE value;

		value = streamPtr[position];

		position++;

		return value;
	}

	BYTE* ReadBytes(int count)
	{
		BYTE* value = new BYTE[count];
		
		memcpy(value, &streamPtr[position], count);

		position += count;

		return value;
	}

	char ReadChar()
	{
		char value;

		value = (char)streamPtr[position];
		
		position++;

		return value;
	}

	char* ReadChars(int count)
	{
		char* value = new char[count];

		memcpy(value, &streamPtr[position], count);

		position += count;

		return value;
	}

	short ReadInt16()
	{
		short value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	int ReadInt32()
	{
		int value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	float ReadSingle()
	{
		float value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);
		
		return value;
	}

	double ReadDouble()
	{
		double value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);
		
		return value;
	}

	unsigned int ReadUInt32()
	{
		unsigned int value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	std::string ReadString()
	{
		std::string value;

		return value;
	}

private:
	BYTE* streamPtr;
	int streamSize;
	int position;
	bool isAllocated = false;

};

void BayerToRGB(BYTE* src, BYTE* dst, int width, int height)
{
	byte r, g, b;

	int strideRaw = width;
	int strideColor = width * 3;

	for (int y = 1; y < (height - 2); y++)
	{
		int C = (y * strideColor + 3);
		for (int x = 1; x < (width - 2); x += 2)
		{
			b = (byte)((
				src[((y - 1) * strideRaw) + (x)] +
				src[((y + 1) * strideRaw) + (x)]
				) / 2);
			g = src[(y * strideRaw) + (x)];
			r = (byte)((
				src[(y * strideRaw) + (x - 1)] +
				src[(y * strideRaw) + (x + 1)]
				) / 2);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;

			b = (byte)((
				src[((y - 1) * strideRaw) + (x)] +
				src[((y - 1) * strideRaw) + (x + 2)] +
				src[((y + 1) * strideRaw) + (x)] +
				src[((y + 1) * strideRaw) + (x + 2)]
				) / 4);
			g = (byte)((
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[(y * strideRaw) + (x)] +
				src[(y * strideRaw) + (x + 2)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 4);
			r = src[(y * strideRaw) + (x + 1)];
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;
		}

		C = (++y * strideColor + 3);
		for (int x = 1; x < (width - 2); x += 2)
		{
			b = src[(y * strideRaw) + (x)];
			g = (byte)((
				src[((y - 1) * strideRaw) + (x)] +
				src[(y * strideRaw) + (x - 1)] +
				src[(y * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x)]
				) / 4);
			r = (byte)((
				src[((y - 1) * strideRaw) + (x - 1)] +
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x - 1)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 4);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;

			b = (byte)((
				src[(y * strideRaw) + (x)] +
				src[(y * strideRaw) + (x + 2)]
				) / 2);
			g = src[(y * strideRaw) + (x + 1)];
			r = (byte)((
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 2);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;
		}
	}
}

void receiveThread(SOCKET hSocket)
{
	int Length = 0;
	BYTE* receiveBuff = new BYTE[1024 * 1024];

	while (true)
	{
		Length = recv(hSocket, (char*)receiveBuff, 1024 * 1024 - 1, 0);
		printf("length : %d\r\n", Length);

		Sleep(100);
	}
}


int saveBMP(BYTE* pData, int bitCount, DWORD nWidth, DWORD nHeight, const char *pBmpName)
{
	BITMAPFILEHEADER  file_h;
	BITMAPINFOHEADER  info_h;
	DWORD             dwBmpSize = 0;
	DWORD             dwRawSize = 0;
	DWORD             dwLine = 0;
	long              lCount, i;
	FILE              *out;
	RGBQUAD           rgbPal[256];

	out = fopen(pBmpName, "wb");

	if (out == NULL)
		return -1;

	file_h.bfType = 0x4D42;
	file_h.bfReserved1 = 0;
	file_h.bfReserved2 = 0;
	file_h.bfOffBits = sizeof(rgbPal) + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	info_h.biSize = sizeof(BITMAPINFOHEADER);
	info_h.biWidth = (DWORD)nWidth;
	info_h.biHeight = (DWORD)nHeight;
	info_h.biPlanes = 1;
	info_h.biBitCount = bitCount;
	info_h.biCompression = BI_RGB;
	info_h.biXPelsPerMeter = 0;
	info_h.biYPelsPerMeter = 0;
	info_h.biClrUsed = 0;
	info_h.biClrImportant = 0;

	dwLine = ((((info_h.biWidth * info_h.biBitCount) + 31) &~31) >> 3);
	dwBmpSize = dwLine * info_h.biHeight;
	info_h.biSizeImage = dwBmpSize;
	file_h.bfSize = dwBmpSize + file_h.bfOffBits + 2;
	

	dwRawSize = info_h.biWidth*info_h.biHeight * (info_h.biBitCount / 8);

	int stride = info_h.biWidth * (info_h.biBitCount / 8);

	if (pData)
	{
		for (i = 0; i < 256; i++)
		{
			rgbPal[i].rgbRed = (BYTE)(i % 256);
			rgbPal[i].rgbGreen = rgbPal[i].rgbRed;
			rgbPal[i].rgbBlue = rgbPal[i].rgbRed;
			rgbPal[i].rgbReserved = 0;
		}

		fwrite((char *)&file_h, 1, sizeof(BITMAPFILEHEADER), out);
		fwrite((char *)&info_h, 1, sizeof(BITMAPINFOHEADER), out);
		fwrite((char *)rgbPal, 1, sizeof(rgbPal), out);
		lCount = dwRawSize;

		for (lCount -= (long)stride; lCount >= 0; lCount -= (long)stride)
		{
			fwrite((pData + lCount), 1, (long)dwLine, out);
		}

	}
	fclose(out);

	return 0;
}

void GetPacketResultBuffer(BYTE* src, BYTE* dst, int srcSize)
{
	int resultSize = srcSize - sizeof(PacketHeader) - sizeof(PacketTail);
	
	memcpy(dst, src + sizeof(PacketHeader), resultSize);	
}

void ReadPacketBodyResult(BYTE* buff, int size)
{
	PacketBodyResultStart* bodyStart = (PacketBodyResultStart*)buff;

	MemoryReadWriter reader(buff, size);
	
	reader.ReadBytes(sizeof(PacketBodyResultStart));

	int count = bodyStart->WorkCount;

	for (int i = 0; i < count; i++)
	{
		PacketBodyResultMiddle* middle = (PacketBodyResultMiddle*)reader.ReadBytes(sizeof(PacketBodyResultMiddle));

		switch (middle->WorkType)
		{
		case WORK_PRESENCE_BRIGHTNESS:
		case WORK_PRESENCE_CONTRAST:
		case WORK_PRESENCE_AREA:
		case WORK_MEASURE_LENGTH:
		case WORK_MEASURE_ANGLE:
		case WORK_MEASURE_OBJECT_COUNT:
		case WORK_COLOR_AREA:
		case WORK_COLOR_OBJECT_COUNT:			
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_PRESENCE_EDGE:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_MEASURE_DIAMETER:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_PRESENCE_SHAPE_COMPARISON:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_ALIGNMENT_GHT:			
			printf("WORK TYPE : %d, RESULT VALUE : %d[ X:%d, Y:%d, R:%.2f ]\n", middle->WorkType, middle->result.ResultValue, middle->result.MatchPosX, middle->result.MatchPosY, (middle->result.MatchAngle / 3.14) * 180);
			break;
		case WORK_COLOR_IDENTIFICATION:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		}
	}

	int imageWidth = bodyStart->ImageWidth;
	int imageHeight = bodyStart->ImageHeight;
	
	BYTE* RawData = new BYTE[imageWidth * imageHeight * 3];
	BayerToRGB(bodyStart->PixelData, RawData, imageWidth, imageHeight);

	saveBMP(RawData, 24, imageWidth, imageHeight, "test.bmp");

	delete[] RawData;
}


int main(int argc, char *argv[])
{
	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN	servAddr;

	BYTE* receiveBuff = new BYTE[1024 * 1024];
	BYTE* resultBuff = new BYTE[1024 * 1024];
		
	int Length = 0;

	argv[1] = ipAddress;
	argv[2] = (char*)"8000";

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		ErrorHandling("WSAStartup() error!");

	hSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (hSocket == INVALID_SOCKET)
		ErrorHandling("socket() error");


	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.S_un.S_addr = inet_addr(argv[1]);
	servAddr.sin_port = htons(atoi(argv[2]));



	if (connect(hSocket, (SOCKADDR*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR)
		ErrorHandling("connect() error!");
	else
		printf("connect() success\n");

	const int nLength = 1024;

	BYTE *data = new BYTE[nLength];

	BYTE START_CODE[8] = { 60, 255, 0, 255, 0, 255, 0, 62 };
	BYTE END_CODE[8] = { 60, 0, 255, 0, 255, 0, 255, 62 };
	int dataSize = sizeof(int);
	int packetSize = HEADER_SIZE + dataSize;
	int MessageType = 512;
	int sendData = 0;


	ZeroMemory(data, sizeof(BYTE) * 32);
	memcpy(&data[0], &START_CODE, sizeof(BYTE) * 8);
	memcpy(&data[8], &packetSize, sizeof(BYTE));
	memcpy(&data[12], &MessageType, sizeof(int));
	memcpy(&data[16], &dataSize, sizeof(int));
	memcpy(&data[20], &sendData, sizeof(int));
	memcpy(&data[24], &END_CODE, sizeof(BYTE) * 8);
	 
	MemoryReadWriter writer(nLength);
	
	writer.WriteBytes(START_CODE, sizeof(START_CODE));
	writer.WriteInt32(packetSize);
	writer.WriteInt32(MessageType);
	writer.WriteInt32(dataSize);
	writer.WriteInt32(sendData);
	writer.WriteBytes(END_CODE, sizeof(END_CODE));

	auto stream = writer.getPtr();

	auto ret = memcmp(stream, data, writer.getSize());

	if (send(hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");



	//std::thread thread1(receiveThread, hSocket);
	//thread1.join();
	

	MemoryReadWriter receiver(1024 * 1024);

	bool receivedStartCode = false;
	while (true)
	{
		Sleep(1);
		Length = recv(hSocket, (char*)receiveBuff, 1024 * 1024 - 1, 0);
		//printf("length: %d\r\n", Length);

		if (Length == -1)
			ErrorHandling("read() error!");

		
		// 1. START CODE 들어오면
		// 2. START FLAG = TRUE
		// 3. END CODE가 들어올 떄까지 누적

		if (memcmp(receiveBuff, START_CODE, sizeof(START_CODE)) == 0)
		{
			receivedStartCode = true;

			receiver.resetPosition();
		}

		if (receivedStartCode)
		{
			receiver.WriteBytes(receiveBuff, Length);
		}

		if (memcmp(receiveBuff + Length - sizeof(END_CODE), END_CODE, sizeof(END_CODE)) == 0)
		{
			//PacketTail* tail = (PacketTail*)(receiveBuff + Length - sizeof(END_CODE));

			receivedStartCode = false;

			PacketHeader* header = (PacketHeader*)receiver.getPtr();

			switch (header->Message)
			{
			case 101:
				printf("Hand Shake ACK\n");
				Sleep(1000);

				if (send(hSocket, (const char*)data, nLength, 0) == -1)
					printf("send() error \n");
				break;

			case 403:
				printf("WORK GROUP READ_ACK\n");

				break;
			case 500:
				printf("RESULT RECEIVE PACKET SIZE: %d\n", receiver.getSize());

				GetPacketResultBuffer((BYTE*)receiver.getPtr(), resultBuff, receiver.getSize());

				ReadPacketBodyResult(resultBuff, receiver.getSize());
								
				break;
			case 513:
				printf("SC_RESULT_SEND_START_ACK\n");

				break;
			}
		}

	}


	closesocket(hSocket);
	WSACleanup();

	delete[] receiveBuff;
	delete[] resultBuff;
	delete[] data;


	return 0;
}

void ErrorHandling(const char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}

//
//class VGValue
//{
//public:
//	WORK_TYPE type;
//
//	virtual std::string ToString() = 0;	
//};
//
//class VGValueInt32 : public VGValue
//{
//public:
//	int GetValue()
//	{
//		return value;
//	}
//
//	std::string ToString()
//	{
//		char buf[256];
//
//		sprintf(buf, "%d", value);
//		return std::string(buf);
//	}
//
//private:
//	int value;
//};
//
//class VGWork
//{
//
//public:
//	const char* GetName();
//	void SetName(const char* name);
//
//public:
//public:
//	int AddItem(const char* name, VGValue* value);
//
//	int GetItemCount();
//	const char* GetItemName(int index);
//	VGValue* GetItemValue(int index);
//
//
//	
//};
//
//class VGResult
//{
//public:
//	int GetWorkCount();
//
//	bool AddWork(VGWork work);
//
//	VGWork GetWork(int index);
//	bool SetWork(int index, VGWork work);	
//};
//
////void SetUserCallback(MyCallback)
////{
////	UserCallback = MyCallback;
////}
//
//void OnReceive(BYTE* resultBuffer, int resultBufferSize)
//{
//	VGResult result;
//
//	// stream => structure
//
//	int workCount = 4;
//
//	{
//		int resultValue = 100;
//		int minThreshold= 50;
//		int maxThreshold = 150;
//		VGWork work;
//
//		work.SetName("밝기");
//		//work.AddItem("밝기값", new VGValueInt32(resultValue));
//		//work.AddItem("최소밝기값", new VGValueInt32(minThreshold));
//		//work.AddItem("최대값기값", new VGValueInt32(maxThreshold));
//
//		result.AddWork(work);
//	}
//
//	{
//
//	}
//
//	// ...
//
//	//UserCallback(&result);
//}
//
//
//
//struct VGWorkResultLocated
//{
//
//};
//
//struct VGWorkResultBrightness
//{
//	int brightnessValue;
//	int mixThreshold;
//	int maxThreshold;
//};
//
//struct VGWorkResultEdge
//{
//
//};
//
//struct VGWorkResult
//{
//
//};
//
//void MyCallback2(VGWorkResult* pResult)
//{
//
//}
//
//
//void MyCallback(VGResult* pResult)
//{
//	for (int i = 0; i < pResult->GetWorkCount(); ++i)
//	{
//		VGWork work = pResult->GetWork(i);
//
//		printf(work.GetName());
//		for (int j = 0; j < work.GetItemCount(); ++j)
//		{
//			auto itemName = work.GetItemName(j);
//			auto itemValue = work.GetItemValue(j);
//
//			printf("%s\t%s\r\n", itemName, itemValue->ToString());
//
//			int value = atoi(itemValue->ToString().c_str());
//
//
//
//
//			/*switch (itemValue->type)
//			{
//			case INT32:
//				int value = ((VGValueInt32*)itemValue)->GetValue();
//				break;
//			}*/
//
//			WORK_TYPE type;
//
//	/*		switch (type)
//			{
//			case BRIGHTNESS:
//				brightnessValue = _resultValue;
//				minThreshold 
//
//
//			default:
//				break;
//			}*/
//
//			
//		}
//	}
//}
//void example()
//{
//
//}
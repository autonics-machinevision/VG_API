#include <WinSock2.h>

enum VG_SYSTEM_STATE
{
	IDLE,
	BUSY,
	PROCESS,
};

enum VG_TRRIGER_MODE
{
	FREE_RUN,
	INTERNAL_TRRIGER,
	EXTERNAL_TRRIGER,
};

enum VG_TRRIGER_DELAY_MODE
{
	NOT_USED,
	TIME,
	ENCODER,
};

enum WORK_TYPE
{
	WORK_NONE,
	WORK_PRESENCE_BRIGHTNESS,
	WORK_PRESENCE_CONTRAST,
	WORK_PRESENCE_AREA,
	WORK_PRESENCE_EDGE,
	WORK_PRESENCE_PATTERN_BRIGHTNESS,
	WORK_PRESENCE_PATTERN_ROTATION,
	WORK_MEASURE_LENGTH,
	WORK_MEASURE_ANGLE,
	WORK_MEASURE_DIAMETER,
	WORK_MEASURE_OBJECT_COUNT,
	WORK_ALIGNMENT_SAD,
	WORK_ALIGNMENT_GHT,
	WORK_MATCH_SAD,
	WORK_MATCH_CIRCULAR,
	WORK_MATCH_OBJECT,
	WORK_COLOR_IDENTIFICATION,
	WORK_COLOR_AREA,
	WORK_COLOR_OBJECT_COUNT,
	WORK_PRESENCE_SHAPE_COMPARISON
};

enum VG_ERROR_MESSAGE
{
	ERR_NONE = 0,
	ERR_IMAGE_NULL = 1,
	ERR_IMAGE_SIZE = 2,
	ERR_WORKPARAM_IS_NULL = 3,
	ERR_ROI_SIZE = 4,
	ERR_THRESHOLD = 5,
	ERR_EDGE_COUNT_IS_NULL = 6,
	ERR_DATvgSIZE = 7,
	ERR_TEMPLATE_NULL = 8,
	ERR_TEMPLATE_SIZE = 9,
	ERR_LDR_CHECKSUM = 10,
	ERR_HOUGH_MAX_VOTES = 11,
	ERR_HOUGH_LINE_IS_ZERO = 12,
	ERR_CIRCLE_EDGE_POINT_IS_ZERO = 13,
	ERR_CIRCLE_NOT_FOUND = 14,
	ERR_ILLEGAL_DATA = 15,
	ERR_BUSY = 16,
	ERR_FLASH = 17,
	ERR_OUT_OF_RANGE = 18,
	ERR_UNKNOWN_COMMAND = 19,
	ERR_NOT_ENOUGH_EDGE = 20,
	ERR_WRONG_INTERSECTION = 21,
	ERR_DETERMINANT_IS_ZERO = 22,
	ERR_ALIGNMENT_FAIL = 23
};

enum VG_RESULT_IMAGE_TYPE
{
	ALL,
	PASS,
	FAIL,
	NONE,
};

enum VG_OUTPUT_PULSE_TYPE
{
	TYPE_PULSE,
	TYPE_LATCH
};

enum VG_OUTPUT_DELAY_TYPE
{
	OUTPUT_DELAY_AFTER_INSPECTION,
	OUTPUT_DELAY_AFTER_TRIGGER
};

enum VG_OUTPUT_ALARM
{
	OUTPUT_NONE,
	INVALID_TRIGGER,
	PROCESSING_TIME_EXEEDED,
	WORK_GROUP_SELECTION_ERROR,
	FTP_FILE_TRANSFER_ERROR
};

enum VG_ACTIVE_MODE
{
	ACTIVE_HIGH,
	ACTIVE_LOW
};

enum VG_OUTPUT_TYPE
{
	TYPE_NPN,
	TYPE_PNP
};

enum VG_INPUT_MODE
{
	INPUT_NOT_USED,
	INPUT_TRIGGER,
	INPUT_WORK_GROUP_CHANGE_CLOCK,
	INPUT_WORK_GROUP_CHANGE_DATA,
	INPUT_ENCODER_A,
	INPUT_ENCODER_B,
	INPUT_WORK_GROUP_CHANGE_BIT0,
	INPUT_WORK_GROUP_CHANGE_BIT1,
	INPUT_WORK_GROUP_CHANGE_BIT2,
	INPUT_WORK_GROUP_CHANGE_BIT3,
	INPUT_ALRAM_RESET
};

enum VG_OUTPUT_MODE
{
	OUTPUT_NOT_USED,
	OUTPUT_INSPECTION_COMPLETE,
	OUTPUT_INSPECTION_RESULT,
	OUTPUT_EXTERNAL_LIGHT_TRIGGER,
	OUTPUT_ALRAM,
	OUTPUT_CAMERA_BUSY,
	OUTPUT_WORKGROUP_CHANGE_COMPLETE
};

struct VGFTPInfo
{
	bool OnOff;
	char* IPAddress;
	int Port;
	char* UserName;
	char* Password;
	char* SaveDir;
	char* ImageName;
	char* ImageNameOption0;
	char* ImageNameOption1;
	char* ImageNameOption2;
	char* ImageNameOption3;
	char* ImageNameOption4;
	bool SavePassImage;
};

struct VGInfo
{
	char* IPAddress;
	char* Mask;
	char* Gateway;
	int Port;
	VGFTPInfo FTP;
};

struct VGID
{
	SOCKET hSocket;
	int index;
	char* ModelName;
	VGInfo NetworkInfo;
};

struct VGList
{
	VGID VGID;
	int Count;
};

struct VGLED
{
	bool OnOff;
	bool Brightness;
};

struct VGCamParams
{
	int Gain;
	int ExposureTime;
	VG_TRRIGER_MODE TriggerMode;
	VG_TRRIGER_DELAY_MODE TriggerDelayMode;
	int TriggerDelayTime;
	int FrameRate;
	VGLED Light;
};

struct VGImage
{
	int Width;
	int Height;
	BYTE* PixelData;
};

struct VGWorkGroupInfo
{
	int Index;
	char* Name;
	char* DateTime;
};

struct VGWorkInfo
{
	WORK_TYPE Type;
};

struct VGInputParams
{
	int PortNum;
	VG_INPUT_MODE Mode;
	VG_ACTIVE_MODE Active;
};

struct VGOutputParams
{
	int PortNum;
	VG_OUTPUT_MODE Mode;
	VG_ACTIVE_MODE Active;
	VG_OUTPUT_TYPE Type;
	VG_OUTPUT_ALARM Alaram;
	VG_OUTPUT_PULSE_TYPE Pulse;
	int Delay;
	int Duration;
};

struct VGDIOParams
{
	VGInputParams Input;
	VGOutputParams Output;
};

struct VGWorkGroupParams
{	
	VGWorkGroupInfo WorkGroupInfo;
	BYTE* MasterImage;
	VGWorkInfo WorkInfo;
	VGOutputParams OutputType;
};

struct VGInspectionResult
{
	int MatchPosX;
	int MatchPosY;
	float MatchAngle;
	int Similarity;
	float EdgeAX;
	float EdgeAY;
	float EdgeBX;
	float EdgeBY;
	float IntersectAX;
	float IntersectAY;
	float IntersectBX;
	float IntersectBY;
	int CircleCenterX;
	int CircleCenterY;
	int CircleRadius;
	int Roundness;
	int ResultValue;
	int PassFailResult;
	int PassCount;
	int FailCount;
	float ProcessingTime;
	VG_ERROR_MESSAGE ErrorMessage;			
};





// System
VGID				  vgInit(SOCKET hSocket, BYTE* Address, int PortNumber);
bool				  vgOpen(VGID);
bool				  vgClose(VGID);
bool				  vgCloseALL();
bool				  vgIsConnected(VGID);
VGList*				  vgFindDeviceList();
int					  vgGetSystemState(VGID);
void				  vgReboot(VGID);
char*				  vgGetProductID(VGID);
VG_SYSTEM_STATE	  vgGetOperationMode(VGID);
bool				  vgSetOperationMode(VGID, VG_SYSTEM_STATE);
void				  vgGetSystemInfo(VGID);
void				  vgGetSystemVer(VGID);
void				  vgSaveDeviceCamParams(VGID);
void				  vgSaveDeviceDIOParams(VGID);
void				  vgSaveDeviceWorkGroupParams(VGID);
void				  vgSaveDeviceNetworkParams(VGID);
char*				  vgGetMACAddress(VGID);
char*				  vgGetSerialNumber(VGID);
void				  vgSendFirmwareFile(VGID);
VGInfo		  vgGetDeviceNetworkInfo(VGID);
void				  vgSetDeviceNetworkInfo(VGID, VGInfo);
void 			      vgSetCallback();			// ���� �ʿ�

// Camera
int					  vgGetCamGainValue(VGID);
void				  vgSetCamGainValue(VGID, int value);
int					  vgGetCamExposureTime(VGID);
void				  vgSetCamExposureTime(VGID, int time);
VG_TRRIGER_MODE		  vgGetCamTriggerMode(VGID);
void				  vgSetCamTriggerMode(VGID, VG_TRRIGER_MODE);
VG_TRRIGER_DELAY_MODE vgGetCamTriggerDelayMode(VGID);
void				  vgSetCamTriggerDelayMode(VGID, VG_TRRIGER_DELAY_MODE);
int					  vgGetCamTriggerDelayTime(VGID);
void				  vgSetCamTriggerDelayTime(VGID, int time);
int					  vgGetCamFrameRate(VGID);
void				  vgSetCamFrameRate(VGID, int fps);
VGCamParams			  vgGetCamParams(VGID);
void				  vgSetCamParams(VGID, VGCamParams);
void				  vgCapture(VGID, VGImage);
bool				  vgBeginCaptureLive(VGID);
bool				  vgEndCaptureLive(VGID);

// LED
int					  vgGetLEDBrightness(VGID);
void				  vgSetLEDBrightness(VGID, int Value);
bool				  vgGetLEDState(VGID);
void				  vgSetLEDState(VGID, bool OnOff);

// Work Group
VGWorkGroupParams	  vgGetWorkGroupParams(VGID, int index);
void				  vgSetWorkGroupParams(VGID, int index, VGWorkGroupParams);
bool				  vgDeleteWorkGroup(VGID, int index);
int					  vgGetActiveWorkGroup(VGID);
bool				  vgSetActiveWorkGroup(VGID, int index);
VGWorkGroupInfo*	  vgGetWorkGroupInfo(VGID);
VGInspectionResult	  vgGetInspectionResult(VGID);
void				  vgSetResultImageViewType(VGID, VG_RESULT_IMAGE_TYPE);

// Device
void				  vgSendFTPResult(VGImage);
VGInputParams		  vgGetDeviceInputType(VGID);
void				  vgSetDeviceInputType(VGID, VGInputParams);
VGOutputParams		  vgGetDeviceOutputType(VGID);
void				  vgSetDeviceOutputType(VGID, VGOutputParams);
VGDIOParams			  vgGetDIOParams(VGID);
void				  vgSetDIOParams(VGID, VGDIOParams);
int					  vgGetEncoderPulsePerUMResolution(VGID);
void				  vgSetEncoderPulsePerUMResolution(VGID, int value);
int					  vgGetEncoderUMPerPulseResolution(VGID);
void				  vgSetEncoderUMPerPulseResolution(VGID, int value);



#pragma once

#include "vgbase.h"

#ifdef __cplusplus
extern "C" {
#endif

	// System
	VGDLL VGHANDLE				vgOpenByIP(const char* IPAddress);
	VGDLL VGHANDLE				vgOpenBySerialNumber(const char* SerialNumber);
	VGDLL VGHANDLE				vgOpenByMacAddress(const char* MACAddress);
	VGDLL bool					vgClose(VGHANDLE handle);
	VGDLL bool					vgCloseAll();
	VGDLL bool					vgIsConnected(VGHANDLE handle);
	VGDLL VGList*				vgFindDeviceList();
	VGDLL int					vgGetSystemState(VGHANDLE handle);
	VGDLL void					vgReboot(VGHANDLE handle);
	VGDLL void					vgCapture(VGHANDLE handle);
	VGDLL void					vgCaptureLive(VGHANDLE handle, bool OnOff);
	VGDLL VG_SYSTEM_STATE		vgGetOperationMode(VGHANDLE handle);
	VGDLL bool					vgSetOperationMode(VGHANDLE handle, VG_SYSTEM_STATE);
	
	// idenfity
	VGDLL unsigned char*		vgGetProductID(VGHANDLE handle);
	VGDLL unsigned char*		vgGetIPAddress(VGHANDLE handle);
	VGDLL int					vgGetPortNumber(VGHANDLE handle);
	VGDLL unsigned char*		vgGetMACAddress(VGHANDLE handle);
	VGDLL unsigned char*		vgGetSerialNumber(VGHANDLE handle);
	VGDLL unsigned char*		vgGetSystemInfo(VGHANDLE handle);
	VGDLL unsigned char*		vgGetFirmwareVersion(VGHANDLE handle);
	
	// save state
	VGDLL void					vgSaveDeviceCamParams(VGHANDLE handle);
	VGDLL void					vgSaveDeviceDIOParams(VGHANDLE handle);
	VGDLL void					vgSaveDeviceWorkGroupParams(VGHANDLE handle);
	VGDLL void					vgSaveDeviceNetworkParams(VGHANDLE handle);
	VGDLL VGInfo				vgGetDeviceNetworkInfo(VGHANDLE handle);
	VGDLL void					vgSetDeviceNetworkInfo(VGHANDLE handle, VGInfo);
	VGDLL void 					vgSetReceiveCallback(InspectionResultReceiveHandler callback, VGHANDLE handle);			// 수정 필요
	VGDLL void					vgGetReceiveData(VGHANDLE handle);
	// Camera parameter
	VGDLL int					vgGetCamGainValue(VGHANDLE handle);
	VGDLL void					vgSetCamGainValue(VGHANDLE handle, int value);
	
	VGDLL int					vgGetCamExposureTime(VGHANDLE handle);
	VGDLL void					vgSetCamExposureTime(VGHANDLE handle, int time);
	
	VGDLL VG_TRRIGER_MODE		vgGetCamTriggerMode(VGHANDLE handle);
	VGDLL void					vgSetCamTriggerMode(VGHANDLE handle, VG_TRRIGER_MODE);
	
	VGDLL VG_TRRIGER_DELAY_MODE vgGetCamTriggerDelayMode(VGHANDLE handle);
	VGDLL void					vgSetCamTriggerDelayMode(VGHANDLE handle, VG_TRRIGER_DELAY_MODE);
	
	VGDLL int					 vgGetCamTriggerDelayTime(VGHANDLE handle);
	VGDLL void					 vgSetCamTriggerDelayTime(VGHANDLE handle, int time);
	
	VGDLL int					 vgGetCamTriggerDelayEncoderPulse(VGHANDLE handle);
	VGDLL void					 vgSetCamTriggerDelayEncoderPulse(VGHANDLE handle, int pulse);
	
	VGDLL int					 vgGetCamFrameRate(VGHANDLE handle);
	VGDLL void					 vgSetCamFrameRate(VGHANDLE handle, int fps);
	
	VGDLL VGCamParams			 vgGetCamParams(VGHANDLE handle);
	VGDLL void					 vgSetCamParams(VGHANDLE handle, VGCamParams);
	
	// LED parameter
	VGDLL int					 vgGetLEDBrightness(VGHANDLE handle);
	VGDLL void					 vgSetLEDBrightness(VGHANDLE handle, int Value);
	VGDLL bool					 vgGetLEDState(VGHANDLE handle);
	VGDLL void					 vgSetLEDState(VGHANDLE handle, bool OnOff);
	
	// Work Group
	VGDLL int					 vgGetWorkGroupParamsCount(VGHANDLE handle);
	VGDLL void					 vgDownloadWorkGroupParams(VGHANDLE handle, int index, const char* workGroupPath);
	VGDLL void					 vgUploadWorkGroupParams(VGHANDLE handle, int index, const char* workGroupPath);
	VGDLL bool					 vgDeleteWorkGroup(VGHANDLE handle, int index);
	VGDLL void					 vgClearWorkGroup(VGHANDLE handle);
	
	// 현재 활성화 된 <= current
	// 부팅시 적용할 <= active
	
	VGDLL int					 vgGetActiveWorkGroup(VGHANDLE handle);
	VGDLL bool					 vgSetActiveWorkGroup(VGHANDLE handle, int index);
	VGDLL VGWorkGroupInfo  		 vgGetWorkGroupInfo(VGHANDLE handle, int index);
	VGDLL void					 vgSetResultImageSavingCondition(VGHANDLE handle, VG_RESULT_IMAGE_SAVING_CONDITION);
	
	VGDLL void 					 vgSetCurrentWorkGroup(VGHANDLE handle, int index);	// index의 WorkGroup 활성화
	VGDLL int					 vgGetCurrentWorkGroup(VGHANDLE handle);				// 현재 활성화 된 WorkGroup의 index 리턴
	
	// Device (구조체 타입으로 이용 후 분리)
	VGDLL VGInputParams			 vgGetDeviceInputType(VGHANDLE handle);
	VGDLL void					 vgSetDeviceInputType(VGHANDLE handle, VGInputParams);
	VGDLL int					 vgGetDeviceInputPortCount(VGHANDLE handle);
	VGDLL VGOutputParams		 vgGetDeviceOutputType(VGHANDLE handle);
	VGDLL void					 vgSetDeviceOutputType(VGHANDLE handle, VGOutputParams);
	VGDLL int					 vgGetDeviceOutputPortCount(VGHANDLE handle);
	VGDLL VGDIOParams			 vgGetDIOParams(VGHANDLE handle);
	VGDLL void					 vgSetDIOParams(VGHANDLE handle, VGDIOParams);

#ifdef __cplusplus
}
#endif
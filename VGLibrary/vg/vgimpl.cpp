#include "stdafx.h"
#include <stdio.h>
#include <WinSock2.h>
#include <stdlib.h>
#include <string>
#include <cstdarg>
#include <thread>

//#include "MemoryReadWriter.h"
#include "vgimpl.h"

#define PORT 8000
#define	IMAGE_WIDTH 752
#define IMAGE_HEIGHT 480

#pragma comment(lib, "ws2_32.lib")

CVGImpl* vgImpl;
int cnt = 0;
static int ActiveWorkGroupIndex = 0;

class MemoryReadWriter
{
public:
	MemoryReadWriter(void* stream, int size)
	{
		SetStream(stream, size);
	}

	MemoryReadWriter(int allocSize)
	{
		allocate(allocSize);
	}

	~MemoryReadWriter()
	{
		release();
	}

public:
	void SetStream(void* stream, int size)
	{
		release();

		streamPtr = (BYTE*)stream;
		streamSize = size;
		position = 0;
		isAllocated = false;
	}

	void allocate(int allocSize)
	{
		release();

		streamPtr = new BYTE[allocSize];
		streamSize = allocSize;
		position = 0;
		isAllocated = true;

	}

	void release()
	{
		if (isAllocated)
		{
			delete[] streamPtr;
		}

		streamPtr = nullptr;
		streamSize = 0;
		position = -1;
		isAllocated = false;
	}

	const BYTE* getPtr()
	{
		return streamPtr;
	}

	int getSize()
	{
		return position;
	}

	void resetPosition()
	{
		position = 0;
	}

public:
	int WriteByte(BYTE value)
	{
		if (position > streamSize) return -1;

		streamPtr[position] = value;

		position++;

		return position;
	}

	int WriteBytes(BYTE* valuePtr, int count)
	{
		if (position > streamSize || count > streamSize - position) return -1;

		memcpy(&streamPtr[position], valuePtr, count);

		position += count;

		return position;
	}

	int WriteInt32(int value)
	{
		if (position > streamSize) return -1;

		memcpy(&streamPtr[position], &value, sizeof(value));

		position += sizeof(value);

		return position;
	}

public:
	BYTE ReadByte()
	{
		BYTE value;

		value = streamPtr[position];

		position++;

		return value;
	}

	BYTE* ReadBytes(int count)
	{
		BYTE* value = new BYTE[count];

		memcpy(value, &streamPtr[position], count);

		position += count;

		return value;
	}

	char ReadChar()
	{
		char value;

		value = (char)streamPtr[position];

		position++;

		return value;
	}

	char* ReadChars(int count)
	{
		char* value = new char[count];

		memcpy(value, &streamPtr[position], count);

		position += count;

		return value;
	}

	short ReadInt16()
	{
		short value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	int ReadInt32()
	{
		int value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	float ReadSingle()
	{
		float value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	double ReadDouble()
	{
		double value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	unsigned int ReadUInt32()
	{
		unsigned int value;

		memcpy(&value, &streamPtr[position], sizeof(value));

		position += sizeof(value);

		return value;
	}

	std::string ReadString()
	{
		std::string value;

		return value;
	}

private:
	BYTE* streamPtr;
	int streamSize;
	int position;
	bool isAllocated = false;

};

void BayerToRGB(BYTE* src, BYTE* dst, int width, int height)
{
	BYTE r, g, b;

	int strideRaw = width;
	int strideColor = width * 3;

	for (int y = 1; y < (height - 2); y++)
	{
		int C = (y * strideColor + 3);
		for (int x = 1; x < (width - 2); x += 2)
		{
			b = (BYTE)((
				src[((y - 1) * strideRaw) + (x)] +
				src[((y + 1) * strideRaw) + (x)]
				) / 2);
			g = src[(y * strideRaw) + (x)];
			r = (BYTE)((
				src[(y * strideRaw) + (x - 1)] +
				src[(y * strideRaw) + (x + 1)]
				) / 2);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;

			b = (BYTE)((
				src[((y - 1) * strideRaw) + (x)] +
				src[((y - 1) * strideRaw) + (x + 2)] +
				src[((y + 1) * strideRaw) + (x)] +
				src[((y + 1) * strideRaw) + (x + 2)]
				) / 4);
			g = (BYTE)((
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[(y * strideRaw) + (x)] +
				src[(y * strideRaw) + (x + 2)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 4);
			r = src[(y * strideRaw) + (x + 1)];
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;
		}

		C = (++y * strideColor + 3);
		for (int x = 1; x < (width - 2); x += 2)
		{
			b = src[(y * strideRaw) + (x)];
			g = (BYTE)((
				src[((y - 1) * strideRaw) + (x)] +
				src[(y * strideRaw) + (x - 1)] +
				src[(y * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x)]
				) / 4);
			r = (BYTE)((
				src[((y - 1) * strideRaw) + (x - 1)] +
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x - 1)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 4);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;

			b = (BYTE)((
				src[(y * strideRaw) + (x)] +
				src[(y * strideRaw) + (x + 2)]
				) / 2);
			g = src[(y * strideRaw) + (x + 1)];
			r = (BYTE)((
				src[((y - 1) * strideRaw) + (x + 1)] +
				src[((y + 1) * strideRaw) + (x + 1)]
				) / 2);
			dst[C++] = b;
			dst[C++] = g;
			dst[C++] = r;
		}
	}
}

int saveBMP(BYTE* pData, int bitCount, DWORD nWidth, DWORD nHeight, const char *pBmpName)
{
	BITMAPFILEHEADER  file_h;
	BITMAPINFOHEADER  info_h;
	DWORD             dwBmpSize = 0;
	DWORD             dwRawSize = 0;
	DWORD             dwLine = 0;
	long              lCount, i;
	FILE              *out;
	RGBQUAD           rgbPal[256];

	out = fopen(pBmpName, "wb");

	if (out == NULL)
		return -1;

	file_h.bfType = 0x4D42;
	file_h.bfReserved1 = 0;
	file_h.bfReserved2 = 0;
	file_h.bfOffBits = sizeof(rgbPal) + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	info_h.biSize = sizeof(BITMAPINFOHEADER);
	info_h.biWidth = (DWORD)nWidth;
	info_h.biHeight = (DWORD)nHeight;
	info_h.biPlanes = 1;
	info_h.biBitCount = bitCount;
	info_h.biCompression = BI_RGB;
	info_h.biXPelsPerMeter = 0;
	info_h.biYPelsPerMeter = 0;
	info_h.biClrUsed = 0;
	info_h.biClrImportant = 0;

	dwLine = ((((info_h.biWidth * info_h.biBitCount) + 31) &~31) >> 3);
	dwBmpSize = dwLine * info_h.biHeight;
	info_h.biSizeImage = dwBmpSize;
	file_h.bfSize = dwBmpSize + file_h.bfOffBits + 2;


	dwRawSize = info_h.biWidth*info_h.biHeight * (info_h.biBitCount / 8);

	int stride = info_h.biWidth * (info_h.biBitCount / 8);

	if (pData)
	{
		for (i = 0; i < 256; i++)
		{
			rgbPal[i].rgbRed = (BYTE)(i % 256);
			rgbPal[i].rgbGreen = rgbPal[i].rgbRed;
			rgbPal[i].rgbBlue = rgbPal[i].rgbRed;
			rgbPal[i].rgbReserved = 0;
		}

		fwrite((char *)&file_h, 1, sizeof(BITMAPFILEHEADER), out);
		fwrite((char *)&info_h, 1, sizeof(BITMAPINFOHEADER), out);
		fwrite((char *)rgbPal, 1, sizeof(rgbPal), out);
		lCount = dwRawSize;

		for (lCount -= (long)stride; lCount >= 0; lCount -= (long)stride)
		{
			fwrite((pData + lCount), 1, (long)dwLine, out);
		}
	}
	fclose(out);

	return 0;
}

int GetPacketResultBuffer(BYTE* src, BYTE* dst, int srcSize)
{
	PacketHeader* header = (PacketHeader*)src;
	
	memcpy(dst, src + sizeof(PacketHeader), header->DataSize);

	return header->DataSize;
}


void ReadPacketBodyResult(BYTE* buff, int size, VGTotalResult* result)
{	

	PacketBodyResultStart* bodyStart = (PacketBodyResultStart*)buff;

	MemoryReadWriter reader(buff, size);

	reader.ReadBytes(sizeof(PacketBodyResultStart));

	int count = bodyStart->WorkCount;

	for (int i = 0; i < count; i++)
	{
		PacketBodyResultMiddle* middle = (PacketBodyResultMiddle*)reader.ReadBytes(sizeof(PacketBodyResultMiddle));
		
		switch (middle->WorkType)
		{
		case WORK_PRESENCE_BRIGHTNESS:
		case WORK_PRESENCE_CONTRAST:
		case WORK_PRESENCE_AREA:
		case WORK_MEASURE_LENGTH:
		case WORK_MEASURE_ANGLE:
		case WORK_MEASURE_OBJECT_COUNT:
		case WORK_COLOR_AREA:
		case WORK_COLOR_OBJECT_COUNT:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_PRESENCE_EDGE:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_MEASURE_DIAMETER:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_PRESENCE_SHAPE_COMPARISON:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		case WORK_ALIGNMENT_GHT:
			printf("WORK TYPE : %d, RESULT VALUE : %d[ X:%d, Y:%d, R:%.2f ]\n", middle->WorkType, middle->result.ResultValue, middle->result.MatchPosX, middle->result.MatchPosY, (middle->result.MatchAngle / 3.14) * 180);
			result->ResultValue = middle->result.ResultValue;
			result->MatchPosX = middle->result.MatchPosX;
			result->MatchPosY = middle->result.MatchPosY;
			break;
		case WORK_COLOR_IDENTIFICATION:
			printf("WORK TYPE : %d, RESULT VALUE : %d\n", middle->WorkType, middle->result.ResultValue);
			break;
		}
	}

	int imageWidth = bodyStart->ImageWidth;
	int imageHeight = bodyStart->ImageHeight;

	BYTE* RawData = new BYTE[imageWidth * imageHeight * 3];
	BayerToRGB(bodyStart->PixelData, RawData, imageWidth, imageHeight);
	
	result->VGImage.Width = imageWidth;
	result->VGImage.Height = imageHeight;
	memcpy(result->VGImage.PixelData, RawData, imageWidth * imageHeight * 3);
	
	//char* str = new char[256];

	//sprintf(str, "C:\\image\\test_%d.bmp", cnt);
	//cnt++;
	
	//saveBMP(RawData, 24, imageWidth, imageHeight, str);

	delete[] RawData;
}

int GetActiveWokrGroupIndex(BYTE* buff, int size)
{
	PacketActiveWorkGroup* packet = (PacketActiveWorkGroup*)buff;	

	return packet->group.ActiveWorkGroupIndex;;
}



CVGImpl::CVGImpl()
{
}

CVGImpl::CVGImpl(VGHANDLE handle)
{
	eventHandler = nullptr;
	
}

CVGImpl::~CVGImpl()
{
	eventHandler = nullptr;
	bReceiveReady = false;
}

std::thread Receivethread;

VGHANDLE CVGImpl::vgOpenByIP(const char* IPAddress)
{
	VGInfo *info = (VGInfo*)malloc(sizeof(VGInfo));

	WSADATA wsaData;
	SOCKADDR_IN	servAddr;

	info->IPAddress = (BYTE*)IPAddress;
	info->Port = PORT;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		return NULL;
	}

	info->hSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (info->hSocket == INVALID_SOCKET)
	{
		return NULL;
	}

	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.S_un.S_addr = inet_addr(IPAddress);
	servAddr.sin_port = htons(PORT);

	if (connect(info->hSocket, (SOCKADDR*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR)
	{
		return NULL;
	}

	
	Receivethread = std::thread(ThreadReceivePacket, (VGHANDLE)info);


	return (VGHANDLE)info;
}

VGHANDLE CVGImpl::vgOpenBySerialNumber(const char* SerialNumber)
{
	VGHANDLE handle = NULL;

	return handle;
}

VGHANDLE CVGImpl::vgOpenByMacAddress(const char* MACAddress)
{
	VGHANDLE handle = NULL;
	
	printf("test\n");

	return handle;
}

bool CVGImpl::vgClose(VGHANDLE handle)
{
	if (handle == NULL) return false;

	VGInfo* info = (VGInfo*)handle;

	closesocket(info->hSocket);

	WSACleanup();

	free(handle);

	delete vgImpl;

	return true;

}

unsigned char* CVGImpl::vgGetIPAddress(VGHANDLE handle)
{
	if (handle == NULL) return NULL;
	VGInfo* info = (VGInfo*)handle;

	return info->IPAddress;
}

int	CVGImpl::vgGetPortNumber(VGHANDLE handle)
{
	if (handle == NULL) return -1;
	VGInfo* info = (VGInfo*)handle;

	return info->Port;
}

unsigned char* CVGImpl::vgGetMACAddress(VGHANDLE handle)
{
	if (handle == NULL) return NULL;
	VGInfo* info = (VGInfo*)handle;

	return info->MacAddress;
}

unsigned char* CVGImpl::vgGetSerialNumber(VGHANDLE handle)
{
	if (handle == NULL) return NULL;
	VGInfo* info = (VGInfo*)handle;

	return info->SerialNumber;
}

unsigned char* CVGImpl::vgGetFirmwareVersion(VGHANDLE handle)
{
	if (handle == NULL) return NULL;
	VGInfo* info = (VGInfo*)handle;

	return info->FirmwareVersion;
}

void CVGImpl::vgGetReceiveData(VGHANDLE handle)
{
	printf("receive\n");
}


void CVGImpl::vgSetReceiveCallback(InspectionResultReceiveHandler callback, VGHANDLE handle)
{
	printf("set callback\n");
	vgImpl = new CVGImpl(handle);
	vgImpl->eventHandler = callback;
}


PacketHeader CVGImpl::MakePacketHeader(VG_MESSAGE_ID id, int dataSize)
{
	PacketHeader header;

	header.DataSize = dataSize;
	header.PacketSize = HEADER_SIZE + dataSize;
	header.Message = id;

	return header;
}

void CVGImpl::SetSystemOperationMode(VGHANDLE handle, VG_SYSTEM_OPERATION_MODE mode, int activeIndex)
{
	VGInfo* info = (VGInfo*)handle;
	PacketHeader header = MakePacketHeader(CS_SYSTEM_OPERATION_MODE_WRITE, 8);
	PacketTail tail;
	int sendData = 0;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(mode);
	writer.WriteInt32(activeIndex);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketOperationMode* packet = (PacketOperationMode*)writer.getPtr();
#endif // _DEBUG

	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");

}


void CVGImpl::RequestResultDataReceive(VGHANDLE handle)
{
	VGInfo* info = (VGInfo*)handle;
	PacketHeader header = MakePacketHeader(CS_RESULT_SEND_START, 4);
	PacketTail tail;
	int sendData = 0;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(sendData);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketGeneral* packet = (PacketGeneral*)writer.getPtr();
#endif // _DEBUG
	   
	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");
}

void CVGImpl::RequestActiveWokrGroupIndex(VGHANDLE handle)
{
	VGInfo* info = (VGInfo*)handle;
	PacketHeader header = MakePacketHeader(CS_ACTIVE_WORK_GROUP_READ, 4);
	PacketTail tail;
	int sendData = 0;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(sendData);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketGeneral* packet = (PacketGeneral*)writer.getPtr();
#endif // _DEBUG

	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");
}


bool CVGImpl::vgSetOperationMode(VGHANDLE handle, VG_SYSTEM_STATE mode)
{
	if (handle == NULL) return false;
		   
	switch (mode)
	{
	case IDLE:
		SetSystemOperationMode(handle, SYSTEM_TEACH, ActiveWorkGroupIndex);
		break;
	case BUSY:	
		//RequestResultDataReceive(handle);		
		RequestActiveWokrGroupIndex(handle);		
		break;
	case PROCESS:
		break;
	default:
		break;
	}

	return true;
}

VGHANDLE CVGImpl::getHandle()
{
	return nullptr;
}

VG_MESSAGE_ID CVGImpl::parseMessageID(unsigned char* packet, int packetSize)
{
	PacketHeader* header = (PacketHeader*)packet;
	return header->Message;
}

VGInspectionResult parseResult(unsigned char* packet, int packetSize)
{
	return VGInspectionResult();
}

void CVGImpl::OnSocketReceived(VGHANDLE handle, BYTE* packet, int packetSize)
{
	VG_MESSAGE_ID messageID = parseMessageID(packet, packetSize);
	BYTE* resultBuff = new BYTE[1024 * 1024];
	int resultsize;
		
	VGTotalResult result;
	result.VGImage.Width = 0;
	result.VGImage.Height = 0;
	result.VGImage.PixelData = new BYTE[IMAGE_WIDTH * IMAGE_HEIGHT* 3];

	switch (messageID)
	{
		
	case SC_TOTAL_RESULT:		
		printf("=============SC_TOTAL_RESULT\n");	

		resultsize = GetPacketResultBuffer((BYTE*)packet, resultBuff, packetSize);

		ReadPacketBodyResult(resultBuff, resultsize, &result);

		eventHandler(handle, result);
		break;
	case SC_RESULT_SEND_START_ACK:
		printf("=============SC_RESULT_SEND_START_ACK\n");

		break;
	case SC_CAMERA_SNAP_IMAGE_ACK: // snap
		printf("*************SC_CAMERA_SNAP_IMAGE_ACK\n");

		resultsize = GetPacketResultBuffer((BYTE*)packet, resultBuff, packetSize);

		GetImageBuffer(handle, messageID, resultBuff, resultsize, &result);

		eventHandler(handle, result);

		break;
	case SC_CAMERA_SNAP_IMAGE: // live
		printf("*************SC_CAMERA_SNAP_IMAGE\n");

		
		ResponseLive(handle);

		resultsize = GetPacketResultBuffer((BYTE*)packet, resultBuff, packetSize);

		GetImageBuffer(handle, messageID, resultBuff, resultsize, &result);

		eventHandler(handle, result);

		break;

	case SC_ACTIVE_WORK_GROUP_READ_ACK:
		ActiveWorkGroupIndex = GetActiveWokrGroupIndex((BYTE*)packet, packetSize);

		SetSystemOperationMode(handle, SYSTEM_RUN, ActiveWorkGroupIndex);
		break;
	}

	delete[] result.VGImage.PixelData;
}

void CVGImpl::ThreadReceivePacket(VGHANDLE handle)
{
	int Length = 0;
	BYTE* receiveBuff = new BYTE[1024 * 1024];
	VGInfo* info = (VGInfo*)handle;
	MemoryReadWriter receiver(1024 * 1024);

	BYTE START_CODE[8] = { 60, 255, 0, 255, 0, 255, 0, 62 };
	BYTE END_CODE[8] = { 60, 0, 255, 0, 255, 0, 255, 62 };
	bool receivedStartCode = false;
	
	while (true)
	{
		Length = recv(info->hSocket, (char*)receiveBuff, 1024 * 1024 - 1, 0);

		if (Length != -1)
		{
			// make complete packet
			if (memcmp(receiveBuff, START_CODE, sizeof(START_CODE)) == 0)
			{
				receivedStartCode = true;

				receiver.resetPosition();
			}

			if (receivedStartCode)
			{
				receiver.WriteBytes(receiveBuff, Length);
			}

			if (memcmp(receiveBuff + Length - sizeof(END_CODE), END_CODE, sizeof(END_CODE)) == 0)
			{
				receivedStartCode = false;

				vgImpl->OnSocketReceived(handle, (BYTE*)receiver.getPtr(), receiver.getSize());
			}
		}

		Sleep(1);
	}
}

void CVGImpl::vgCapture(VGHANDLE handle)
{
	VGInfo* info = (VGInfo*)handle;
	PacketHeader header = MakePacketHeader(CS_CAMERA_SNAP_IMAGE, 4);
	PacketTail tail;
	int sendData = 0;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(sendData);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketGeneral* packet = (PacketGeneral*)writer.getPtr();
#endif // _DEBUG

	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");
}

void CVGImpl::vgCaptureLive(VGHANDLE handle, bool OnOff)
{
	VGInfo* info = (VGInfo*)handle;
	VG_MESSAGE_ID id = OnOff ? CS_CAMERA_LIVE_START : CS_CAMERA_LIVE_END;
	PacketHeader header = MakePacketHeader(id, 4);
	PacketTail tail;
	int sendData = 0;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(sendData);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketGeneral* packet = (PacketGeneral*)writer.getPtr();
#endif // _DEBUG

	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");
}

void CVGImpl::ResponseLive(VGHANDLE handle)
{
	VGInfo* info = (VGInfo*)handle;
	VG_MESSAGE_ID id = CS_CAMERA_SNAP_IMAGE_ACK;
	PacketHeader header = MakePacketHeader(id, 4);
	PacketTail tail;
	VG_ERROR_MESSAGE err = ERR_NONE;

	MemoryReadWriter writer(BUFFER_LENGTH);

	writer.WriteBytes(header.StartCode, sizeof(header.StartCode));
	writer.WriteInt32(header.PacketSize);
	writer.WriteInt32(header.Message);
	writer.WriteInt32(header.DataSize);
	writer.WriteInt32(err);
	writer.WriteBytes(tail.EndCode, sizeof(tail.EndCode));

#ifdef _DEBUG
	PacketGeneral* packet = (PacketGeneral*)writer.getPtr();
#endif // _DEBUG

	if (send(info->hSocket, (const char*)writer.getPtr(), writer.getSize(), 0) == -1)
		printf("send() error \n");

	printf("*************CS_CAMERA_SNAP_IMAGE_ACK\n");
}

void CVGImpl::GetImageBuffer(VGHANDLE handle, VG_MESSAGE_ID id, BYTE* buff, int buffsize, VGTotalResult* result)
{
	int size = buffsize;
	MemoryReadWriter reader(buff, buffsize);

	if (id == SC_CAMERA_SNAP_IMAGE_ACK)
	{
		reader.ReadInt32();
		size = buffsize - sizeof(int);
	}	

	int imageWidth = reader.ReadInt32();
	int imageHeight = reader.ReadInt32();

	BYTE* pixelData = new BYTE[imageWidth * imageHeight];

	pixelData = reader.ReadBytes(imageWidth * imageHeight);

	BYTE* RawData = new BYTE[imageWidth * imageHeight * 3];

	BayerToRGB(pixelData, RawData, imageWidth, imageHeight);

	result->VGImage.Width = imageWidth;
	result->VGImage.Height = imageHeight;
	memcpy(result->VGImage.PixelData, RawData, imageWidth * imageHeight * 3);

	//char* str = new char[256];

	//sprintf(str, "C:\\image\\test_%d.bmp", cnt);
	//cnt++;

	//saveBMP(RawData, 24, imageWidth, imageHeight, str);

	delete[] RawData;
	delete[] pixelData;
}


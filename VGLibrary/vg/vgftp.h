#pragma once

struct VGFTPInfo
{
	bool OnOff;
	char* IPAddress;
	int Port;
	char* UserName;
	char* Password;
	char* SaveDir;
	char* ImageName;
	char* ImageNameOption0;
	char* ImageNameOption1;
	char* ImageNameOption2;
	char* ImageNameOption3;
	char* ImageNameOption4;
	bool SavePassImage;
};
﻿// vg.cpp : DLL 응용 프로그램을 위해 내보낸 함수를 정의합니다.
//

#include "stdafx.h"
#include "vg.h"
#include "vgimpl.h"


CVGImpl g_impl;

VGHANDLE vgOpenByIP(const char* IPAddress)
{
	return g_impl.vgOpenByIP(IPAddress);
}

VGHANDLE vgOpenBySerialNumber(const char* SerialNumber)
{
	return g_impl.vgOpenBySerialNumber(SerialNumber);
}

VGHANDLE vgOpenByMacAddress(const char* MACAddress)
{
	return g_impl.vgOpenByMacAddress(MACAddress);
}

bool vgClose(VGHANDLE handle)
{
	return g_impl.vgClose(handle);
}

unsigned char* vgGetIPAddress(VGHANDLE handle)
{
	return g_impl.vgGetIPAddress(handle);
}

int	vgGetPortNumber(VGHANDLE handle)
{
	return g_impl.vgGetPortNumber(handle);
}

unsigned char* vgGetMACAddress(VGHANDLE handle)
{
	return g_impl.vgGetMACAddress(handle);
}

unsigned char* vgGetSerialNumber(VGHANDLE handle)
{
	return g_impl.vgGetSerialNumber(handle);
}

unsigned char* vgGetFirmwareVersion(VGHANDLE handle)
{
	return g_impl.vgGetFirmwareVersion(handle);
}

void vgGetReceiveData(VGHANDLE handle)
{
	return g_impl.vgGetReceiveData(handle);
}

void vgSetReceiveCallback(InspectionResultReceiveHandler callback, VGHANDLE handle)
{
	return g_impl.vgSetReceiveCallback(callback, handle);
}

bool vgSetOperationMode(VGHANDLE handle, VG_SYSTEM_STATE mode)
{
	return g_impl.vgSetOperationMode(handle, mode);
}

void vgCapture(VGHANDLE handle)
{
	return g_impl.vgCapture(handle);
}

void vgCaptureLive(VGHANDLE handle, bool OnOff)
{
	return g_impl.vgCaptureLive(handle, OnOff);
}
#pragma once

#ifdef __cplusplus
extern "C" {
#endif
	
	typedef unsigned char BYTE;

	typedef struct MemoryReadWriter
	{
		BYTE* streamPtr;
		int streamSize;
		int position;
		bool isAllocated = false;
	} MemoryReadWriter, *pMemoryReadWriter;

	
	// Control
	static MemoryReadWriter* SetStream(void* stream, int size);
	static MemoryReadWriter* Allocate(int allocSize);
	void Release(MemoryReadWriter* rw);
	const BYTE* getPtr(MemoryReadWriter* writer);
	int getSize(MemoryReadWriter* rw);
	void resetPosition(MemoryReadWriter* rw);

	// Write
	int WriteByte(MemoryReadWriter* writer, BYTE value);
	int WriteBytes(MemoryReadWriter* writer, BYTE* valuePtr, int count);
	int WriteInt32(MemoryReadWriter*writer, int value);

	// Read
	BYTE ReadByte(MemoryReadWriter* reader);
	BYTE* ReadBytes(MemoryReadWriter* reader, int count);
	char ReadChar(MemoryReadWriter* reader);
	char* ReadChars(MemoryReadWriter* reader, int count);
	short ReadInt16(MemoryReadWriter* reader);
	int ReadInt32(MemoryReadWriter* reader);
	float ReadSingle(MemoryReadWriter* reader);
	double ReadDouble(MemoryReadWriter* reader);
	unsigned int ReadUInt32(MemoryReadWriter* reader);

#ifdef __cplusplus
}
#endif

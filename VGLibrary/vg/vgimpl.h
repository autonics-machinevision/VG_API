#pragma once
#include "vgbase.h"

class CVGImpl
{
public:
	VGHANDLE				vgOpenByIP(const char* IPAddress);
	VGHANDLE				vgOpenBySerialNumber(const char* SerialNumber);
	VGHANDLE				vgOpenByMacAddress(const char* MACAddress);
	bool					vgClose(VGHANDLE handle);
	bool					vgCloseAll();
	bool					vgIsConnected(VGHANDLE handle);
	VGList*					vgFindDeviceList();
	int						vgGetSystemState(VGHANDLE handle);
	void					vgReboot(VGHANDLE handle);
	void					vgCapture(VGHANDLE handle);
	void					vgCaptureLive(VGHANDLE handle, bool OnOff);	
	VG_SYSTEM_STATE			vgGetOperationMode(VGHANDLE handle);
	bool					vgSetOperationMode(VGHANDLE handle, VG_SYSTEM_STATE);

	// idenfity
	unsigned char*			vgGetProductID(VGHANDLE handle);
	unsigned char*			vgGetIPAddress(VGHANDLE handle);
	int						vgGetPortNumber(VGHANDLE handle);
	unsigned char*			vgGetMACAddress(VGHANDLE handle);
	unsigned char*			vgGetSerialNumber(VGHANDLE handle);
	unsigned char*			vgGetSystemInfo(VGHANDLE handle);
	unsigned char*			vgGetFirmwareVersion(VGHANDLE handle);

	void					vgSaveDeviceCamParams(VGHANDLE handle);
	void					vgSaveDeviceDIOParams(VGHANDLE handle);
	void					vgSaveDeviceWorkGroupParams(VGHANDLE handle);
	void					vgSaveDeviceNetworkParams(VGHANDLE handle);
	VGInfo					vgGetDeviceNetworkInfo(VGHANDLE handle);	
	void					vgSetDeviceNetworkInfo(VGHANDLE handle, VGInfo);
	void 					vgSetReceiveCallback(InspectionResultReceiveHandler callback, VGHANDLE handle);			// 수정 필요
	void					vgGetReceiveData(VGHANDLE handle);
	int						vgGetCamGainValue(VGHANDLE handle);
	void					vgSetCamGainValue(VGHANDLE handle, int value);

	int						vgGetCamExposureTime(VGHANDLE handle);
	void					vgSetCamExposureTime(VGHANDLE handle, int time);

	VG_TRRIGER_MODE			vgGetCamTriggerMode(VGHANDLE handle);
	void					vgSetCamTriggerMode(VGHANDLE handle, VG_TRRIGER_MODE);

	VG_TRRIGER_DELAY_MODE	vgGetCamTriggerDelayMode(VGHANDLE handle);
	void					vgSetCamTriggerDelayMode(VGHANDLE handle, VG_TRRIGER_DELAY_MODE);

	int						 vgGetCamTriggerDelayTime(VGHANDLE handle);
	void					 vgSetCamTriggerDelayTime(VGHANDLE handle, int time);

	int						 vgGetCamTriggerDelayEncoderPulse(VGHANDLE handle);
	void					 vgSetCamTriggerDelayEncoderPulse(VGHANDLE handle, int pulse);

	int						 vgGetCamFrameRate(VGHANDLE handle);
	void					 vgSetCamFrameRate(VGHANDLE handle, int fps);

	VGCamParams				 vgGetCamParams(VGHANDLE handle);
	void					 vgSetCamParams(VGHANDLE handle, VGCamParams);

	int						 vgGetLEDBrightness(VGHANDLE handle);
	void					 vgSetLEDBrightness(VGHANDLE handle, int Value);
	bool					 vgGetLEDState(VGHANDLE handle);
	void					 vgSetLEDState(VGHANDLE handle, bool OnOff);

	int						 vgGetWorkGroupParamsCount(VGHANDLE handle);
	void					 vgDownloadWorkGroupParams(VGHANDLE handle, int index, const char* workGroupPath);
	void					 vgUploadWorkGroupParams(VGHANDLE handle, int index, const char* workGroupPath);
	bool					 vgDeleteWorkGroup(VGHANDLE handle, int index);
	void					 vgClearWorkGroup(VGHANDLE handle);


	int						 vgGetActiveWorkGroup(VGHANDLE handle);
	bool					 vgSetActiveWorkGroup(VGHANDLE handle, int index);
	VGWorkGroupInfo  		 vgGetWorkGroupInfo(VGHANDLE handle, int index);
	void					 vgSetResultImageSavingCondition(VGHANDLE handle, VG_RESULT_IMAGE_SAVING_CONDITION);

	void 					 vgSetCurrentWorkGroup(VGHANDLE handle, int index);	// index의 WorkGroup 활성화
	int						 vgGetCurrentWorkGroup(VGHANDLE handle);				// 현재 활성화 된 WorkGroup의 index 리턴

	VGInputParams			 vgGetDeviceInputType(VGHANDLE handle);
	void					 vgSetDeviceInputType(VGHANDLE handle, VGInputParams);
	int						 vgGetDeviceInputPortCount(VGHANDLE handle);
	VGOutputParams			 vgGetDeviceOutputType(VGHANDLE handle);
	void					 vgSetDeviceOutputType(VGHANDLE handle, VGOutputParams);
	int						 vgGetDeviceOutputPortCount(VGHANDLE handle);
	VGDIOParams				 vgGetDIOParams(VGHANDLE handle);
	void					 vgSetDIOParams(VGHANDLE handle, VGDIOParams);
private:
	bool bReceiveReady = false;
	static void ThreadReceivePacket(VGHANDLE handle);
	VGHANDLE getHandle();
	PacketHeader MakePacketHeader(VG_MESSAGE_ID id, int dataSize);
	void OnSocketReceived(VGHANDLE handle, BYTE* packet, int packetSize);
	void RequestResultDataReceive(VGHANDLE handle);
	void SetSystemOperationMode(VGHANDLE handle, VG_SYSTEM_OPERATION_MODE mode, int activeIndex);
	VG_MESSAGE_ID parseMessageID(unsigned char* packet, int packetSize);
	void RequestActiveWokrGroupIndex(VGHANDLE handle);
	void GetImageBuffer(VGHANDLE handle, VG_MESSAGE_ID id, BYTE* buff, int buffsize, VGTotalResult* result);
	void ResponseLive(VGHANDLE handle);
public:
	CVGImpl();
	CVGImpl(VGHANDLE handle);
	~CVGImpl();
private:
	InspectionResultReceiveHandler eventHandler;
};


#include "stdafx.h"
#include "MemoryReadWriter.h"
#include <stdlib.h>


#ifdef __cplusplus
extern "C" {
#endif
	
// Control
static MemoryReadWriter* SetStream(void* stream, int size)
{
	MemoryReadWriter* reader = (MemoryReadWriter*)malloc(sizeof(MemoryReadWriter));

	Release(reader);

	reader->streamPtr = (BYTE*)stream;
	reader->streamSize = size;
	reader->position = 0;
	reader->isAllocated = false;

	return reader;
}

static MemoryReadWriter* Allocate(int allocSize)
{
	MemoryReadWriter* writer = (MemoryReadWriter*)malloc(sizeof(MemoryReadWriter));

	Release(writer);

	writer->streamPtr = new BYTE[allocSize];
	writer->streamSize = allocSize;
	writer->position = 0;
	writer->isAllocated = true;

	return writer;
}

void Release(MemoryReadWriter* rw)
{
	if (rw->isAllocated)
	{
		delete[] rw->streamPtr;
	}

	rw->streamPtr = nullptr;
	rw->streamSize = 0;
	rw->position = -1;
	rw->isAllocated = false;
}

const BYTE* getPtr(MemoryReadWriter* writer)
{
	return writer->streamPtr;
}

int getSize(MemoryReadWriter* rw)
{
	return rw->position;
}

void resetPosition(MemoryReadWriter* rw)
{
	rw->position = 0;
}

// Write
int WriteByte(MemoryReadWriter* writer, BYTE value)
{
	if (writer->position > writer->streamSize) return -1;

	writer->streamPtr[writer->position] = value;

	writer->position++;

	return writer->position;
}

int WriteBytes(MemoryReadWriter* writer, BYTE* valuePtr, int count)
{
	if (writer->position > writer->streamSize || count > writer->streamSize - writer->position) return -1;

	memcpy(&writer->streamPtr[writer->position], valuePtr, count);

	writer->position += count;

	return writer->position;
}

int WriteInt32(MemoryReadWriter* writer, int value)
{
	if (writer->position > writer->streamSize) return -1;

	memcpy(&writer->streamPtr[writer->position], &value, sizeof(value));

	writer->position += sizeof(value);

	return writer->position;
}

// Read
BYTE ReadByte(MemoryReadWriter* reader)
{
	BYTE value;

	value = reader->streamPtr[reader->position];

	reader->position++;

	return value;
}

BYTE* ReadBytes(MemoryReadWriter* reader, int count)
{
	BYTE* value = new BYTE[count];

	memcpy(value, &reader->streamPtr[reader->position], count);

	reader->position += count;

	return value;
}

char ReadChar(MemoryReadWriter* reader)
{
	char value;

	value = (char)reader->streamPtr[reader->position];

	reader->position++;

	return value;
}

char* ReadChars(MemoryReadWriter* reader, int count)
{
	char* value = new char[count];

	memcpy(value, &reader->streamPtr[reader->position], count);

	reader->position += count;

	return value;
}

short ReadInt16(MemoryReadWriter* reader)
{
	short value;

	memcpy(&value, &reader->streamPtr[reader->position], sizeof(value));

	reader->position += sizeof(value);

	return value;
}

int ReadInt32(MemoryReadWriter* reader)
{
	int value;

	memcpy(&value, &reader->streamPtr[reader->position], sizeof(value));

	reader->position += sizeof(value);

	return value;
}

float ReadSingle(MemoryReadWriter* reader)
{
	float value;

	memcpy(&value, &reader->streamPtr[reader->position], sizeof(value));

	reader->position += sizeof(value);

	return value;
}

double ReadDouble(MemoryReadWriter* reader)
{
	double value;

	memcpy(&value, &reader->streamPtr[reader->position], sizeof(value));

	reader->position += sizeof(value);

	return value;
}

unsigned int ReadUInt32(MemoryReadWriter* reader)
{
	unsigned int value;

	memcpy(&value, &reader->streamPtr[reader->position], sizeof(value));

	reader->position += sizeof(value);

	return value;
}

#ifdef __cplusplus
}
#endif

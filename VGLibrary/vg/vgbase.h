#pragma once

#include <WinSock2.h>

#ifdef VG_EXPORTS
#define VGDLL __declspec(dllexport)
#else
#define VGDLL __declspec(dllimport)

#endif // VG_EXPORTS
typedef unsigned char BYTE;
typedef PVOID VGHANDLE;

enum VG_SYSTEM_STATE
{
	IDLE,
	BUSY,
	PROCESS,
};

enum VG_SYSTEM_OPERATION_MODE
{
	SYSTEM_IDLE,
	SYSTEM_TEACH,
	SYSTEM_RUN
};

enum VG_TRRIGER_MODE
{
	FREE_RUN,
	INTERNAL_TRRIGER,
	EXTERNAL_TRRIGER,
};

enum VG_TRRIGER_DELAY_MODE
{
	NOT_USED,
	TIME,
	ENCODER,
};

enum WORK_TYPE
{
	WORK_NONE,
	WORK_PRESENCE_BRIGHTNESS,
	WORK_PRESENCE_CONTRAST,
	WORK_PRESENCE_AREA,
	WORK_PRESENCE_EDGE,
	WORK_PRESENCE_PATTERN_BRIGHTNESS,
	WORK_PRESENCE_PATTERN_ROTATION,
	WORK_MEASURE_LENGTH,
	WORK_MEASURE_ANGLE,
	WORK_MEASURE_DIAMETER,
	WORK_MEASURE_OBJECT_COUNT,
	WORK_ALIGNMENT_SAD,
	WORK_ALIGNMENT_GHT,
	WORK_MATCH_SAD,
	WORK_MATCH_CIRCULAR,
	WORK_MATCH_OBJECT,
	WORK_COLOR_IDENTIFICATION,
	WORK_COLOR_AREA,
	WORK_COLOR_OBJECT_COUNT,
	WORK_PRESENCE_SHAPE_COMPARISON
};

enum VG_ERROR_MESSAGE
{
	ERR_NONE = 0,
	ERR_IMAGE_NULL = 1,
	ERR_IMAGE_SIZE = 2,
	ERR_WORKPARAM_IS_NULL = 3,
	ERR_ROI_SIZE = 4,
	ERR_THRESHOLD = 5,
	ERR_EDGE_COUNT_IS_NULL = 6,
	ERR_DATvgSIZE = 7,
	ERR_TEMPLATE_NULL = 8,
	ERR_TEMPLATE_SIZE = 9,
	ERR_LDR_CHECKSUM = 10,
	ERR_HOUGH_MAX_VOTES = 11,
	ERR_HOUGH_LINE_IS_ZERO = 12,
	ERR_CIRCLE_EDGE_POINT_IS_ZERO = 13,
	ERR_CIRCLE_NOT_FOUND = 14,
	ERR_ILLEGAL_DATA = 15,
	ERR_BUSY = 16,
	ERR_FLASH = 17,
	ERR_OUT_OF_RANGE = 18,
	ERR_UNKNOWN_COMMAND = 19,
	ERR_NOT_ENOUGH_EDGE = 20,
	ERR_WRONG_INTERSECTION = 21,
	ERR_DETERMINANT_IS_ZERO = 22,
	ERR_ALIGNMENT_FAIL = 23
};

enum VG_RESULT_IMAGE_SAVING_CONDITION
{
	ALL,
	PASS,
	FAIL,
	NONE,
};

enum VG_OUTPUT_PULSE_TYPE
{
	TYPE_PULSE,
	TYPE_LATCH
};

enum VG_OUTPUT_DELAY_TYPE
{
	OUTPUT_DELAY_AFTER_INSPECTION,
	OUTPUT_DELAY_AFTER_TRIGGER
};

enum VG_OUTPUT_ALARM
{
	OUTPUT_NONE,                      // 0 
	INVALID_TRIGGER,                  // 1 
	PROCESSING_TIME_EXEEDED,          // 2 
	WORK_GROUP_SELECTION_ERROR,       // 4 
	FTP_FILE_TRANSFER_ERROR           // 8 
};

enum OUTPUT_PORT
{
	OUTPUT_PORT_1,
	OUTPUT_PORT_2,
	OUTPUT_PORT_3,
	OUTPUT_PORT_4
};

enum VG_ACTIVE_MODE
{
	ACTIVE_HIGH,
	ACTIVE_LOW
};

enum VG_OUTPUT_TYPE
{
	TYPE_NPN,
	TYPE_PNP
};

enum VG_INPUT_MODE
{
	INPUT_NOT_USED,
	INPUT_TRIGGER,
	INPUT_WORK_GROUP_CHANGE_CLOCK,
	INPUT_WORK_GROUP_CHANGE_DATA,
	INPUT_ENCODER_A,
	INPUT_ENCODER_B,
	INPUT_WORK_GROUP_CHANGE_BIT0,
	INPUT_WORK_GROUP_CHANGE_BIT1,
	INPUT_WORK_GROUP_CHANGE_BIT2,
	INPUT_WORK_GROUP_CHANGE_BIT3,
	INPUT_ALRAM_RESET
};

enum VG_OUTPUT_MODE
{
	OUTPUT_NOT_USED,
	OUTPUT_INSPECTION_COMPLETE,
	OUTPUT_INSPECTION_RESULT,
	OUTPUT_EXTERNAL_LIGHT_TRIGGER,
	OUTPUT_ALRAM,
	OUTPUT_CAMERA_BUSY,
	OUTPUT_WORKGROUP_CHANGE_COMPLETE
};

enum VG_MESSAGE_ID
{
	// system operate
	CS_SYSTEM_OPERATION_MODE_WRITE = 230,
	SC_SYSTEM_OPERATION_MODE_WRITE_ACK,
	CS_SYSTEM_OPERATION_MODE_READ,
	SC_SYSTEM_OPERATION_MODE_READ_ACK,
	
	// camera
	CS_CAMERA_SNAP_IMAGE = 360,
	SC_CAMERA_SNAP_IMAGE_ACK,
	SC_CAMERA_SNAP_IMAGE,
	CS_CAMERA_SNAP_IMAGE_ACK,
	CS_CAMERA_LIVE_START = 370,
	SC_CAMERA_LIVE_START_ACK,
	CS_CAMERA_LIVE_END,
	SC_CAMERA_LIVE_END_ACK,

	// active workgroup
	CS_ACTIVE_WORK_GROUP_READ = 412,
	SC_ACTIVE_WORK_GROUP_READ_ACK,

	// get total result
	SC_TOTAL_RESULT = 500,

	// request total result
	CS_RESULT_SEND_START = 512,
	SC_RESULT_SEND_START_ACK,
};



typedef struct VGInfo
{
	SOCKET hSocket;
	BYTE* IPAddress;
	BYTE* Mask;
	BYTE* Gateway;
	BYTE* MacAddress;
	BYTE* ModelName;
	BYTE* FirmwareVersion;
	BYTE* SerialNumber;
	int Port;	
} VGInfo;

struct VGID
{
	int index;
	char* ModelName;
};

struct VGList
{
	VGInfo Networkinfo;
	int Count;
};

struct VGLED
{
	bool OnOff;
	bool Brightness;
};

struct VGCamParams
{
	int Gain;
	int ExposureTime;
	VG_TRRIGER_MODE TriggerMode;
	VG_TRRIGER_DELAY_MODE TriggerDelayMode;
	int TriggerDelayTime;
	int FrameRate;
	VGLED Light;
};

struct VGImage
{
	int Width;
	int Height;
	BYTE* PixelData;
};

struct PacketSnapImage
{
	VG_ERROR_MESSAGE err;
	int Width;
	int Height;
	unsigned char* PixelData;
};

struct VGWorkGroupInfo
{
	int Index;
	char* Name;
	char* DateTime;
};

struct VGWorkInfo
{
	WORK_TYPE Type;
};

struct VGInputParams
{
	int PortNum;
	VG_INPUT_MODE Mode;
	VG_ACTIVE_MODE Active;
};

struct VGOutputParams
{
	int PortNum;
	VG_OUTPUT_MODE Mode;
	VG_ACTIVE_MODE Active;
	VG_OUTPUT_TYPE Type;
	VG_OUTPUT_ALARM Alarm;
	VG_OUTPUT_PULSE_TYPE Pulse;
	int Delay;
	int Duration;
};

struct VGDIOParams
{
	VGInputParams Input;
	VGOutputParams Output;
};

struct VGWorkGroupParams
{
	VGWorkGroupInfo WorkGroupInfo;
	unsigned char* MasterImage;
	VGWorkInfo WorkInfo;
	VGOutputParams OutputType;
};

struct VGActiveWorkGroup
{
	VG_ERROR_MESSAGE error;
	int ActiveWorkGroupIndex;
};

struct VGOperationMode
{
	VG_SYSTEM_OPERATION_MODE mode;
	int ActiveWorkGroupIndex;
};


struct VGInspectionResult
{
	int MatchPosX;
	int MatchPosY;
	float MatchAngle;
	int Similarity;
	float EdgeAX;
	float EdgeAY;
	float EdgeBX;
	float EdgeBY;
	float IntersectAX;
	float IntersectAY;
	float IntersectBX;
	float IntersectBY;
	int CircleCenterX;
	int CircleCenterY;
	int CircleRadius;
	int Roundness;
	int ResultValue;
	int PassFailResult;
	int PassCount;
	int FailCount;
	float ProcessingTime;
	VG_ERROR_MESSAGE ErrorMessage;
};

struct VGTotalResult
{
	VGImage VGImage;
	int MatchPosX;
	int MatchPosY;
	float MatchAngle;
	int Similarity;
	float EdgeAX;
	float EdgeAY;
	float EdgeBX;
	float EdgeBY;
	float IntersectAX;
	float IntersectAY;
	float IntersectBX;
	float IntersectBY;
	int CircleCenterX;
	int CircleCenterY;
	int CircleRadius;
	int Roundness;
	int ResultValue;
	int PassFailResult;
	int PassCount;
	int FailCount;
	float ProcessingTime;
	VG_ERROR_MESSAGE ErrorMessage;
};

#define HEADER_SIZE 8 + 4 + 4 + 4 + 8
#define BUFFER_LENGTH 1024 * 1024


struct PacketHeader
{
	BYTE StartCode[8] = { 60, 255, 0, 255, 0, 255, 0, 62 };
	int PacketSize;
	VG_MESSAGE_ID Message;
	int DataSize;
};

struct PacketBody
{
	int Data;
};

struct PacketTail
{
	BYTE EndCode[8] = { 60, 0, 255, 0, 255, 0, 255, 62 };
};



struct PacketGeneral
{
	PacketHeader header;
	PacketBody body;
	PacketTail tail;
};

struct PacketBodyResultStart
{
	int saveLocation;
	int ImageWidth;
	int ImageHeight;
	BYTE PixelData[752 * 480];
	int TriggerPassCount;
	int TriggerFailCount;
	int WorkCount;
};

struct PacketBodyResultMiddle
{
	WORK_TYPE WorkType;	// work data start
	VGInspectionResult result;
};
struct PacketBodyResultEnd
{
	int Output[4];
	int FirstRho;
	int FirstVotes;
	int FirstTheta;
	int SecondRho;
	int SecondVotes;
	int SecondTheta;
	int TotalProcessTime;
	int TotalPassCount;
	int TotalFailCount;
};

struct PacketResult
{
	PacketBodyResultStart bodyStart;
	PacketBodyResultMiddle bodyMiddle;
	PacketBodyResultEnd bodyEnd;
};

struct PacketOperationMode
{
	PacketHeader header;
	VGOperationMode mode;
	PacketTail tail;
};

struct PacketActiveWorkGroup
{
	PacketHeader header;
	VGActiveWorkGroup group;
	PacketTail tail;
};




typedef VOID(*InspectionResultReceiveHandler)(VGHANDLE, VGTotalResult);
